MESAJ="SpaceUserBot Otomatik Deploy Quruluma Xoş Geldiniz"
MESAJ+="\nTelegram: @SpaceUserBot"
pkg update -y
clear
echo -e $MESAJ
echo "Python Yüklenir"
pkg install python -y
clear
echo -e $MESAJ
echo "Git Yüklenir"
pkg install git -y
clear
echo -e $MESAJ
echo "TeleThon Yüklenir"
pip install telethon
echo "Repo klonlanıyor..."
git clone https://github.com/WhoMiri/SpaceInstaller
clear
echo -e $MESAJ
cd SpaceInstaller
clear
echo "Bezi Alt Yapi Dosyalari Yüklenir"
echo -e $MESAJ
pip install wheel
pip install -r requirements.txt
python -m space_installer
